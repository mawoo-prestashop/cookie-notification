class CookieNotification {
  constructor(cookieName, cookieValue, daysToExpire) {
    this.cookieNotification = document.getElementById("cookie-notification");
    this.cookieNotificationActive = "cookie-notification_active";
    this.cookieNotificationButton = document.getElementById(
      "cookie-notification-button"
    );
    this.cookieName = cookieName;
    this.cookieValue = cookieValue;
    this.daysToExpire = daysToExpire;
  }

  events() {
    this.cookieNotificationButton.addEventListener("click", () => {
      this.setCookie(this.cookieName, this.cookieValue, this.daysToExpire);
    });
  }

  showCookieNotification() {
    this.cookieNotification.classList.add(this.cookieNotificationActive);
  }

  hideCookieNotification() {
    this.cookieNotification.classList.remove(this.cookieNotificationActive);
  }

  setCookie(cname, cvalue, exdays) {
    const d = new Date();
    d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);
    const expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";

    this.hideCookieNotification();
  }

  getCookie(cname, cvalue) {
    const cookiesArray = document.cookie.split(";");
    const cookieNamePattern = new RegExp(cname + "=" + cvalue);

    for (let i = 0; i < cookiesArray.length; i++) {
      const cookie = cookiesArray[i];

      if (cookieNamePattern.test(cookie)) {
        return true;
      }
    }

    return false;
  }

  checkCookie() {
    let cookieNotification = this.getCookie(this.cookieName, this.cookieValue);

    if (!cookieNotification) {
      this.showCookieNotification();
    }
  }

  init() {
    this.events();
    this.checkCookie();
  }
}

document.addEventListener("DOMContentLoaded", () => {
  const cookieNotification = new CookieNotification(
    "COOKIE_NOTIFICATION",
    1,
    365
  );

  cookieNotification.init();
});
