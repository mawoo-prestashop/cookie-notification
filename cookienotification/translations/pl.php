<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{cookienotification}prestashop>cookienotification_a131b1e70dd8b0c1f0912995281aaeb4'] = 'Powiadomienie o plikach cookie';
$_MODULE['<{cookienotification}prestashop>cookienotification_ee78e92bbc626014fe4a8c048dd6b44b'] = 'Powiadomienie o plikach cookie pozwala poinformować użytkowników, że Twoja witryna wykorzystuje pliki cookie i jest zgodna z unijnymi przepisami dotyczącymi plików cookie w odniesieniu do GDPR.';
$_MODULE['<{cookienotification}prestashop>cookienotification_876f23178c29dc2552c0b48bf23cd9bd'] = 'Jesteś pewien, że chcesz odinstalować?';
$_MODULE['<{cookienotification}prestashop>cookienotification_0f40e8817b005044250943f57a21c5e7'] = 'Nie podano nazwy';
$_MODULE['<{cookienotification}prestashop>cookienotification_fe5d926454b6a8144efce13a44d019ba'] = 'Nieprawidłowa wartość konfiguracji';
$_MODULE['<{cookienotification}prestashop>cookienotification_c888438d14855d7d96a2724ee9c306bd'] = 'Ustawienia zaktualizowane';
$_MODULE['<{cookienotification}prestashop>cookienotification_f4f70727dc34561dfde1a3c529b6205c'] = 'Ustawienia';
$_MODULE['<{cookienotification}prestashop>cookienotification_c9cc8cce247e49bae79f15173ce97354'] = 'Zapisz';
$_MODULE['<{cookienotification}prestashop>cookienotification_630f6dc397fe74e52d5189e2c80f282b'] = 'Powrót do listy';
$_MODULE['<{cookienotification}prestashop>cookienotification_4a56d683621c0ed81c9d9ff2a028c204'] = 'Akceptuję pliki Cookie';
