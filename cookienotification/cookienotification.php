<?php
/*
*  @author Maciej Wójcik <>
*  @copyright 2019
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class CookieNotification extends Module
{
    public function __construct()
    {
        $this->name = 'cookienotification';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Maciej Wójcik';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = [
          'min' => '1.6',
          'max' => _PS_VERSION_
      ];
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('Cookie notification');
        $this->description = $this->l('Cookie notification allows you to inform users that your site uses cookies and to comply with the EU cookie law GDPR regulations.');

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

        if (!Configuration::get('COOKIE_NOTIFICATION')) {
            $this->warning = $this->l('No name provided');
        }
    }

    public function install()
    {
        if (Shop::isFeatureActive()) {
            Shop::setContext(Shop::CONTEXT_ALL);
        }

        if (!parent::install() ||
        !$this->registerHook('displayBeforeBodyClosingTag') ||
        !$this->registerHook('header') ||
          !Configuration::updateValue('COOKIE_NOTIFICATION', '<p>This site uses cookies. By using this website you agree to the use of cookies. More information can be found in the <a href="#">Privacy Policy</a>.</p>', true)
      ) {
            return false;
        }

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
          !Configuration::deleteByName('COOKIE_NOTIFICATION')
      ) {
            return false;
        }

        return true;
    }

    public function getContent()
    {
        $output = null;

        if (Tools::isSubmit('submit'.$this->name)) {
            $cookienotification = Tools::getValue('COOKIE_NOTIFICATION');

            if (
                !$cookienotification ||
                empty($cookienotification)
            ) {
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            } else {
                Configuration::updateValue('COOKIE_NOTIFICATION', $cookienotification, true);
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }

        return $output.$this->displayForm();
    }

    public function displayForm()
    {
        // Get default language
        $defaultLang = (int)Configuration::get('PS_LANG_DEFAULT');

        // Init Fields form array
        $fieldsForm[0]['form'] = [
            'tinymce' => true,
            'legend' => [
                'title' => $this->l('Settings'),
            ],
            'input' => [
                [
                    'type' => 'textarea',
                    'label' => $this->l('Cookie notification'),
                    'name' => 'COOKIE_NOTIFICATION',
                    'cols' => 30,
                    'rows' => 10,
                    'class' => 'rte',
                    'autoload_rte' => true,
                    'required' => true
                ]
            ],
            'submit' => [
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            ]
        ];

        $helper = new HelperForm();

        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

        // Language
        $helper->default_form_language = $defaultLang;
        $helper->allow_employee_form_lang = $defaultLang;

        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = [
            'save' => [
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ],
            'back' => [
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            ]
        ];

        // Load current value
        $helper->fields_value['COOKIE_NOTIFICATION'] = Configuration::get('COOKIE_NOTIFICATION');

        return $helper->generateForm($fieldsForm);
    }

    public function hookDisplayBeforeBodyClosingTag($params)
    {
        $this->context->smarty->assign([
        'cookie_notification' => Configuration::get('COOKIE_NOTIFICATION'),
      ]);

        return $this->display(__FILE__, 'cookienotification.tpl');
    }
    
    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'views/css/cookienotification.css', 'all');
        $this->context->controller->addJS($this->_path.'views/js/cookienotification.js');
    }
}
